﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransitionImage : MonoBehaviour {

    private float velTransicion;
    private float alpha;
    private Image image;

    private GameController gc;

    void Awake()
    {
        gc = GameObject.FindWithTag("GameController").GetComponent<GameController>();
        velTransicion = gc.velTransicion;
    }

    public IEnumerator Fade(bool oscurecer)
    {
        image = GetComponent<Image>();

        if (oscurecer)
        {
            alpha = 1;
            while (image.color.a > 0.01f)
            {
                alpha += -(1 / velTransicion) * Time.deltaTime;
                image.color = new Color(1, 1, 1, alpha);
                yield return null;
            }
            image.color = new Color(1, 1, 1, 0);
        }
        else
        {
            alpha = 0;
            while (image.color.a < 0.99f)
            {
                alpha += (1 / velTransicion) * Time.deltaTime;
                image.color = new Color(1, 1, 1, alpha);
                yield return null;
            }
            image.color = new Color(1, 1, 1, 1);
        }
    }

    public IEnumerator Move(bool subir, float distancia)
    {
        if(subir)
        {
            Vector2 posDestino = transform.position + new Vector3(0, distancia, 0);
            while(transform.position.y + velTransicion * Time.deltaTime < posDestino.y)
            {
                transform.position += new Vector3(0f, (distancia / velTransicion) * Time.deltaTime, 0);
                yield return null;
            }
            transform.position = posDestino;
        }
        else
        {
            Vector2 posDestino = transform.position - new Vector3(0, distancia, 0);
            while (transform.position.y - velTransicion * Time.deltaTime > posDestino.y)
            {
                transform.position += new Vector3(0f, -(distancia / velTransicion) * Time.deltaTime, 0);
                yield return null;
            }
            transform.position = posDestino;
        }
    }
}
