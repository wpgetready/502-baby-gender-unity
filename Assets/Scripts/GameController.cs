using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;

public class GameController : MonoBehaviour
{
	public SimpleObjectPool answerButtonObjectPool;
	public Text questionText;
	public Text scoreDisplay;
	public Transform answerButtonParent;

	public GameObject questionDisplay;
	public GameObject roundEndDisplay;
    public Text EndText;
    public Image imagenDeTransicion;
    public float velTransicion;
    
	private DataController dataController;
	private RoundData currentRoundData;
	private QuestionData[] questionPool;

	private bool isRoundActive = false;
	private float timeRemaining;
	private float playerScore;
	private int questionIndex;
    private float alpha;
	private List<GameObject> answerButtonGameObjects = new List<GameObject>();
    public Image QuestionImage;//~robo, image thing.
	void Start()
	{
		dataController = FindObjectOfType<DataController>();								// Store a reference to the DataController so we can request the data we need for this round

		currentRoundData = dataController.GetCurrentRoundData();							// Ask the DataController for the data for the current round. At the moment, we only have one round - but we could extend this
		questionPool = currentRoundData.questions;											// Take a copy of the questions so we could shuffle the pool or drop questions from it without affecting the original RoundData object
		playerScore = 0;
		questionIndex = 0;

		ShowQuestion();
		isRoundActive = true;
	}

	void Update()
	{
        QuestionData questionData = questionPool[questionIndex];
        QuestionImage.overrideSprite = Resources.Load<Sprite>(questionData.questionImage);
        //Debug.Log(questionPool.Length);
    }
    /*
           _           
          | |          
 _ __ ___ | |__   ___  
| '__/ _ \| '_ \ / _ \ 
| | | (_) | |_) | (_) |
|_|  \___/|_.__/ \___/  //added image.
*/
    void ShowQuestion()
	{
		RemoveAnswerButtons();

		QuestionData questionData = questionPool[questionIndex];							// Get the QuestionData for the current question
		questionText.text = questionData.questionText;										// Update questionText with the correct text
       //~robo

        for (int i = 0; i < questionData.answers.Length; i ++)								// For every AnswerData in the current QuestionData...
		{
			GameObject answerButtonGameObject = answerButtonObjectPool.GetObject();			// Spawn an AnswerButton from the object pool
			answerButtonGameObjects.Add(answerButtonGameObject);
			answerButtonGameObject.transform.SetParent(answerButtonParent);
			answerButtonGameObject.transform.localScale = Vector3.one;

			AnswerButton answerButton = answerButtonGameObject.GetComponent<AnswerButton>();
			answerButton.SetUp(questionData.answers[i]);									// Pass the AnswerData to the AnswerButton so the AnswerButton knows what text to display and whether it is the correct answer
		}
	}

	void RemoveAnswerButtons()
	{
		while (answerButtonGameObjects.Count > 0)											// Return all spawned AnswerButtons to the object pool
		{
			answerButtonObjectPool.ReturnObject(answerButtonGameObjects[0]);
			answerButtonGameObjects.RemoveAt(0);
		}
	}

    /*
            _           
           | |          
  _ __ ___ | |__   ___  
 | '__/ _ \| '_ \ / _ \ 
 | | | (_) | |_) | (_) |
 |_|  \___/|_.__/ \___/  //changed bool to float.
 */
	public IEnumerator AnswerButtonClicked(float AddScore)
    { 
		playerScore += AddScore;					// If the AnswerButton that was clicked was the correct answer, add points
		scoreDisplay.text = "Score:  "+playerScore.ToString();

        StartCoroutine(QuestionImage.GetComponent<TransitionImage>().Fade(true));
        StartCoroutine(questionText.GetComponent<TransitionImage>().Move(true, 100));
        StartCoroutine(answerButtonParent.GetComponent<TransitionImage>().Move(false, 400));

        yield return new WaitForSeconds(velTransicion + 0.1f); // es la velocidad de las transiciones + 0.1f (margen de error)
        

		if(questionPool.Length > questionIndex + 1)											// If there are more questions, show the next question
		{
			questionIndex++;
			ShowQuestion();

            StartCoroutine(QuestionImage.GetComponent<TransitionImage>().Fade(false));
            StartCoroutine(questionText.GetComponent<TransitionImage>().Move(false, 100));
            StartCoroutine(answerButtonParent.GetComponent<TransitionImage>().Move(true, 400));

            yield return new WaitForSeconds(velTransicion + 0.1f); // es la velocidad de las transiciones + 0.1f (margen de error)
        }
		else																				// If there are no more questions, the round ends
		{
			EndRound();
            yield return StartCoroutine(Transicion(false));
        }
	}

    /*
           _           
          | |          
 _ __ ___ | |__   ___  
| '__/ _ \| '_ \ / _ \ 
| | | (_) | |_) | (_) |
|_|  \___/|_.__/ \___/  //changed bool to float.
*/
    public void EndRound()
	{
        
		isRoundActive = false;
       
        if (playerScore < 0) {
            EndText.text ="IT'S A GIRL" ;
        }
        if (playerScore > 0)
        {
            EndText.text = "IT'S A BOY";
        }
        else if (playerScore == 0)
        {
            EndText.text = "IT'S A ZIR !!??";//Pls change this :) ~robo for more information on zir -> https://en.wiktionary.org/wiki/zir //no racism :D
        }
		questionDisplay.SetActive(false);
		roundEndDisplay.SetActive(true);
	}

	public void ReturnToMenu()
	{
		SceneManager.LoadScene("MenuScreen");
	}

    private IEnumerator Transicion(bool oscurecer)
    {
        if(oscurecer)
        {
            while (imagenDeTransicion.color.a < 0.99f)
            {
                alpha += (1 / velTransicion) * Time.deltaTime;
                imagenDeTransicion.color = new Color(0, 0, 0, alpha);
                yield return null;
            }
            imagenDeTransicion.color = Color.black;
        }
        else
        {
            while(imagenDeTransicion.color.a > 0.01f)
            {
                alpha += -(1 / velTransicion) * Time.deltaTime;
                imagenDeTransicion.color = new Color(0, 0, 0, alpha);
                yield return null;
            }
            imagenDeTransicion.color = Color.clear;
        }
    }
}
 