﻿[System.Serializable]
public class QuestionData
{
	public string questionText;
    public string questionImage;
    public AnswerData[] answers;
}